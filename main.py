from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP
import rng as rng

generate = 0
private_key = 0
public_key = 0
fToEncrypt = 0
while 1:
    print()
    print('1 - Create new generator')
    if generate:
        print('2 - Generate new key pair')
    if public_key:
        print('3 - Encrypt file')
    if fToEncrypt:
        print('4 - Decrypt file')
    print('5 - Exit')
    print()
    option = input("Choose option: ")

    if option == '1':
        try:
            generate = rng.RNG()
        except:
            print("Couldn't find file")
        else:
            print("Ready")
    elif option == '2' and generate:
        try:
            key = RSA.generate(1024, generate.getNextRandom)
            private_key = key.export_key()
            f_out = open("private.pem", "wb")
            f_out.write(private_key)
            f_out.close()
            public_key = key.publickey().export_key()
            f_out = open("public.pem", "wb")
            f_out.write(public_key)
            f_out.read
            f_out.close()
        except:
            print("Something went wrong")
        else:
            print("New keys generated")
    elif option == '3':
        fToEncrypt = input("Input file name you want to encrypt: ")
        fData = 0
        try:
            with open(fToEncrypt, 'rb') as f:
                fData = f.read()
        except:
            print("Couldn't find file")
        else:
            f_out = open(f"{fToEncrypt}_encrypted.bin", "wb")
            recipient_key = RSA.import_key(open("public.pem").read())
            session_key = generate.getNextRandom(16)
            cipher_rsa = PKCS1_OAEP.new(recipient_key)
            enc_session_key = cipher_rsa.encrypt(session_key)
            cipher_aes = AES.new(session_key, AES.MODE_EAX)
            ciphertext, tag = cipher_aes.encrypt_and_digest(fData)
            [f_out.write(x) for x in (enc_session_key, cipher_aes.nonce, tag, ciphertext)]
            f_out.close()
    elif option == '4' and fToEncrypt and private_key:
        try:
            private_key = RSA.import_key(open('private.pem').read())
            f_in = open(f"{fToEncrypt}_encrypted.bin", "rb")
            enc_session_key, nonce, tag, ciphertext = \
                [f_in.read(x) for x in (private_key.size_in_bytes(), 16, 16, -1)]
            f_in.close()
        except:
            print(f"Wrong key, otherwise check if file path: {fToEncrypt}_encrypted.bin is valid")
        else:
            cipher_rsa = PKCS1_OAEP.new(private_key)
            session_key = cipher_rsa.decrypt(enc_session_key)
            cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
            data = cipher_aes.decrypt_and_verify(ciphertext, tag)
            saveName = input("Save decrypted file as: ")
            f_out = open(f"{saveName}", "wb")
            f_out.write(data)
            f_out.close()
            print(f"File saved as {saveName}")
    elif option == '5':
        print()
        print("End of program")
        break
